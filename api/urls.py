from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('studentapi/', views.StudentList.as_view()),
    # path('studentapi/', views.StudentCreate.as_view()),
    # path('studentapi/<int:pk>', views.StudentRetreive.as_view()),
    path('studentapi/<int:pk>', views.StudentUpdate.as_view()),

]
